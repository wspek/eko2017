"""
 Created by waldo on 9/3/17
"""

import os
import shutil

__author__ = "waldo"
__project__ = "blockly-games"

SRC_FOLDER = "/media/waldo/DATA-SHARE/Code/eko2017/blockly-games/appengine/"  # The appengine folder
DST_FOLDER = "/media/waldo/DATA-SHARE/Code/eko2017/blockly-games/spyvsspy/"


def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)

def mkdir(folder):
    dir = os.path.dirname(folder)
    if not os.path.exists(dir):
        os.makedirs(dir)

mkdir(DST_FOLDER)
shutil.copy(SRC_FOLDER + "maze.html", DST_FOLDER)
shutil.copy(SRC_FOLDER + "maze/NOTICE.txt", DST_FOLDER)
mkdir(DST_FOLDER + "maze/generated/en/")
shutil.copy(SRC_FOLDER + "/maze/generated/en/compressed.js", DST_FOLDER + "maze/generated/en/")
mkdir(DST_FOLDER + "common/")
copytree(SRC_FOLDER + "common/", DST_FOLDER + "common/")
mkdir(DST_FOLDER + "/third-party/blockly/media/")
copytree(SRC_FOLDER + "third-party/blockly/media/", DST_FOLDER + "/third-party/blockly/media/")
mkdir(DST_FOLDER + "/third-party/JS-Interpreter/")
shutil.copy(SRC_FOLDER + "/third-party/JS-Interpreter/compiled.js", DST_FOLDER + "/third-party/JS-Interpreter/")
shutil.copy(SRC_FOLDER + "maze/bg_spy.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/fail_spy.mp3", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/fail_spy.ogg", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/win_spy.mp3", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/win_spy.ogg", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/help_down.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/help_run.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/help_stack.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/help_up.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/icons.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/icons.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/marker.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/spy_whitehat.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/spy_blackhat.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/spy_tileset_320x256.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/start.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/index.png", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/spyvsspy.mp3", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/spyvsspy.ogg", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "maze/style.css", DST_FOLDER + "/maze/")
shutil.copy(SRC_FOLDER + "spy.html", DST_FOLDER + "../index.html")

# Copy game files
